GIAODIEN = '''+-------------------Menu------------------+

1. Load data from file and display

2. Input & add to the end.

3. Display data

4. Save product list to file.

5. Search by ID

6. Delete by ID

7. Sort by ID.

8. Convert to Binary 

9. Load to stack and display

10. Load to queue and display.

0. Exit

+-----------------------------------------.+'''

TitleMenu = "ID |  Title   | Quantity | price"
Output = "output.txt"

class Product:
    def __init__(self,string):
        s = string.split(",")
        self.ID = s[0]
        self.Title = s[1]
        self.quantity = int(s[2])
        self.price = float(s[3])

    def __str__(self):
        string = f'''{self.ID}  |   {self.Title}    |   {self.quantity} |   {self.price}'''
        return string

class Node:
    def __init__(self,product):
        self.product =product
        self.next = None

    def __str__(self):
        return str(self.product)

class MyList:
    def __init__(self,Node):
        self.head = Node

    def printList(self):
        print(TitleMenu)
        temp = self.head
        while (temp):
            print(temp)
            temp = temp.next

    def insert(self,Node):
        if self.head is None:
            self.head = Node
            return
        lastNode = self.head
        while (lastNode.next is not None):
            lastNode = lastNode.next
        lastNode.next = Node

    def saveList(self,filepath):
        with open(filepath, "w", encoding="utf-8") as f:
            f.writelines(TitleMenu)
            f.write("\n")
            last = self.head
            while (last):
                f.write(str(last))
                f.write("\n")
                last = last.next

    def searchByID(self,ID):
        last = self.head
        while (last):
            if last.product.ID == ID:
                return last
            last = last.next
        return None

    def deleteByID(self,ID):
        temp = self.head
        if (temp is not None):
            if (temp.product.ID == ID):
                self.head = temp.next
                return temp
        while (temp is not None):
            if temp.product.ID == ID:
                break
            prev = temp
            temp = temp.next
        prev.next = temp.next
        return temp

    def sortById(self):
        ElementNumber = self.CountElement()
        head = None
        for item in range(ElementNumber):
            sortNode = self.head
            minNode = sortNode
            while (sortNode):
                if sortNode.product.ID < minNode.product.ID:
                    minNode = sortNode
                sortNode = sortNode.next
            newNode = Node(minNode.product)
            self.deleteByID(minNode.product.ID)
            if (head is None):
                head = newNode
                currentB = head
            else:
                currentB.next = newNode
                currentB = currentB.next
        self.head = head

    def CountElement(self):
        if self.head == None:
            return 0
        temp = self.head
        i=0
        while (temp is not None):
            i+=1
            temp = temp.next
        return i


class MyStack(MyList):
    def __init__(self):
        self.head = None

    def delete(self):
        if self.head == None:
            return None
        last = self.head
        if last.next == None:
            self.head = None
            return last
        if last.next.next == None:
            lastNode = last.next
            last.next = None
            return lastNode
        while (last.next.next):
            last = last.next
        delete_Node = last.next
        last.next = None
        return delete_Node

class MyQueue(MyList):
    def __init__(self):
        self.head = None

    def pop(self):
        if self.head is None:
            return None
        else:
            temp = self.head
            self.head = self.head.next
            return temp

class OperationToProduct():
    @staticmethod
    def load_du_lieu(input):
        with open(input, "r", encoding="utf-8") as f:
            data = f.readlines()
        product = Product(data[0])
        new_node = Node(product)
        listproduct = MyList(new_node)
        last = listproduct.head
        for item in data[1:]:
            product = Product(item)
            newNode = Node(product)
            last.next = newNode
            last = last.next
        return listproduct

        # hàm Swap nhận vào 1 node và trả ra node được swap. Nếu gọi hàm này nghĩa là swap với hàm kế bên nó.
    @staticmethod
    def swapNode(Node1):
        Node2 = Node1.next
        tmp = Node2.next
        Node2.next = Node1
        Node1.next = tmp
        return Node2

def AS2_Main():
    list_product = OperationToProduct.load_du_lieu('input.txt')
    while True:
        print(GIAODIEN)
        try:
            rep = int(input())
        except:
            rep = -1
        if rep not in range(1, 11):
            print("Vui long chon dung Menu")
            continue
        if rep == 0:
            print("Thanks")
            break
        if rep == 1:
            try:
                path = (input("Please enter the find path: ..."))
                list_product = OperationToProduct.load_du_lieu(path)
                print("The file is loaded successfully!")
            except:
                print("File-path is not correct")
        if rep == 2:
            try:
                ID = input("Please enter the new product ID: ...")
                name = input("Please enter the product's name: ...")
                quantity = input("Please enter the product's quantity: ...")
                price = input("Please enter the product's price ...")
                stringProduct = f'''{ID},{name},{quantity},{price}'''
                product = Product(stringProduct)
                newNode = Node(product)
                list_product.insert(newNode)
            except:
                print("Information is not correct")
        if rep == 3:
            try:
                list_product.printList()
            except:
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 4:
            try:
                path = (input("Please enter the output path: ..."))
                list_product.saveList(path)
                print("The file is saved successfully!!")
            except:
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 5:
            try:
                id = input("Please enter the ID: ...")
                product = list_product.searchByID(id)
                if product is not None:
                    print(str(product))
                else:
                    print("ID is not in the dataset!")
            except:
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 6:
            try:
                id = input("Please enter the ID: ...")
                product = list_product.deleteByID(id)
                if product is not None:
                    print(str(product))
                    print("The product is removed from the dataset successfully!")
                else:
                    print("ID is not in the dataset!")
            except Exception as e:
                print(e)
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 7:
            try:
                list_product.sortById()
                list_product.printList()
            except Exception as e:
                print(e)
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 8:
            try:
                id = input("Please enter the ID: ...")
                product = list_product.searchByID(id)
                if product is not None:
                    print(str(product))
                    print("Convert quantity to binary: ",str(bin(product.product.quantity))[2:])
                else:
                    print("ID is not in the dataset!")
            except Exception as e:
                print(e)
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 10:
            try:
                current = list_product.head
                ProductStack = MyStack()
                while (current is not None):
                    newNode = current.product
                    ProductStack.insert(Node(newNode))
                    current = current.next
                ProductStack.printList()
                print(TitleMenu)
                product = ProductStack.delete()
                while (product is not None):
                    print(str(product))
                    product = ProductStack.delete()
            except Exception as e:
                print(e)
                print("The List is not exits. Please chooose 1 to load the list first")
        if rep == 9:
            try:
                current = list_product.head
                ProductQueue = MyQueue()
                while (current is not None):
                    newNode = current.product
                    ProductQueue.insert(Node(newNode))
                    current = current.next
                ProductQueue.printList()
                print(TitleMenu)
                product = ProductQueue.pop()
                while (product is not None):
                    print(str(product))
                    product = ProductQueue.pop()
                ProductQueue.printList()
            except Exception as e:
                print(e)
                print("The List is not exits. Please chooose 1 to load the list first")

if __name__ == "__main__":
    AS2_Main()