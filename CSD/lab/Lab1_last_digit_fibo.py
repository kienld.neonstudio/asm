def check_last_digit_Fibo(n):
# chu so cuoi cung cua so fibo thu n
	if n < 0:
		return -1
	if n == 1:
		return 0
	if n == 2:
		return 1

	f1 = 0
	f2 = 1
	f = 0
	for i in range(2,n+1):
		f = (f1%10 + f2%10)%10
		f1 = f2
		f2 = f

	return f

print(check_last_digit_Fibo(327305))
# 0 1 1 2 3 