def xeploai_list(l):
	dtb_chuan = ( (l[0]+l[4]+l[5])*2+(l[1]+l[2]+l[3]+l[6]+l[7]) )/11.0
	
	if dtb_chuan > 9:
		for i in l:
			if i < 8.0:
				return "Gioi"
		return "Xuat sac"
	
	if dtb_chuan > 8.0 and dtb_chuan <=9.0:
		for i in l:
			if i < 6.5:
				return "Kha"
		return"Gioi"

	if dtb_chuan > 6.5 and dtb_chuan <= 8.0:
		for i in l:
			if i < 5.0:
				return "TB kha"
		return "Kha"

	return "TB"

def xeploai_hocsinh(dsach):
	temp = dsach.copy()
	for i in temp:
		temp[i] = xeploai_list(temp[i])
	return temp


def xepLoaiTuNhien(diem):
	if diem >=24:
		return 1
	if diem >=18 and diem < 24:
		return 2
	if diem <18 and diem >=12 :
		return 3
	return 4

def xepLoaiXaHoi(diem):
	if diem >=21:
		return 1
	if diem >=15 and diem < 21:
		return 2
	if diem <15 and diem >=12 :
		return 3
	return 4

def xepLoaiCoBan(diem):
	if diem >=32:
		return 1
	if diem >=24 and diem < 32:
		return 2
	if diem <24 and diem >=20 :
		return 3
	return 4

def xeploai_thidaihoc_hocsinh(dsach):
	temp = dsach.copy()
	for i in temp:
		l = temp[i]
		A = l[0] + l[1] + l[2]
		A1=	l[0] + l[1] + l[5]
		B = l[0] + l[2] + l[3]
		C = l[4] + l[5] + l[6]
		D = l[0] + l[4] + l[5]*2

		temp[i]= [xepLoaiTuNhien(A) ,xepLoaiTuNhien(B), xepLoaiTuNhien(A1), xepLoaiXaHoi(C), xepLoaiCoBan(D)]
	return temp



def main():
	try:
		f = open("../Bai1/diem_trungbinh.txt","r")
		l = f.readlines()
		dsach = {}
		# luu thong tin vao 1 dict dsach
		for i in range(1,len(l)):
			l[i] = l[i].strip()
			for j in range(1,len(l[i])):
				if l[i][j].isdigit():
					ten = l[i][0:j].strip()
					dsach[ten] = {}
					diem_mon = l[i][j:].strip().split()
					diem_mon = list(map(float, diem_mon))
					dsach[ten] = diem_mon
					break
                    
		#print(dsach)

		xl = xeploai_hocsinh(dsach)
		dh = xeploai_thidaihoc_hocsinh(dsach)
		print(dh)
		g = open("danhgia_hocsinh.txt","w")
		g.write('Ma HS\t\t\txeploai_TB chuan\t\txeploai_A\t\txeploai_A1\t\txeploai_B\t\txeploai_C\t\txeploai_D')
		
		for i in dsach:
			st = ("\n{0}\t\t\t{1}\t\t\t{2}\t\t\t{3}\t\t\t{4}\t\t\t{5}\t\t\t{6}").format(i,xl[i],dh[i][0],dh[i][1],dh[i][2],dh[i][3],dh[i][4])
			g.write(st)

		
	except BaseException as e:
		print(e)

	finally:
		f.close()
    
if __name__ == "__main__":
    main()