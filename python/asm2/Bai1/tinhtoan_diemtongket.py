def tinhdiem_trungbinh(l):
    #ham tinh trung binh diem
    if len(l) == 4:
        return round(l[0]*5/100 + l[1]*10/100 + l[2]*15/100 + l[3]*70/100,2)
    if len(l) == 5:
        return round((l[0]*5/100 + l[1]*10/100 + l[2]*10/100 + l[3]*15/100 + l[4]*60/100) ,2)

def luudiem_trungbinh(dsach):
        try:
            f = open("diem_trungbinh.txt","w")
            f.write('MSHS\t\t\tToan\t\tLy\t\tHoa\t\tSinh\t\tVan\t\tAnh\t\tSu\t\tDia')
            for i in dsach:
                f.write("\n"+str(i)+"\t\t")
                for j in dsach[i]:
                    f.write(str(dsach[i][j])+"\t\t")
        except:
            print("loi ghi file")
        finally:
            f.close()
def main():
    try:
        f = open("diem_chitiet.txt","r")
        l = f.readlines()
    
        # luu thong tin vao 1 dict dsach
        ten_mon = l[0].split()
        ten_mon.pop(0)
        dsach = {}
        
        for i in range(1,len(l)):
            
            l[i] = l[i].strip()
            
            for j in range(len(l[i])):
                if l[i][j].isdigit():
                    ten = l[i][0:j].strip()
                    dsach[ten]={}
                    diem_mon = l[i][j:].strip().split(";")
                    
                    for m in range(len(diem_mon)):
                        diem_mon[m] = diem_mon[m].strip().split(",")
                        diem_mon[m] = list(map(float, diem_mon[m]))
                        dsach[ten][ten_mon[m]] = diem_mon[m]
                    break

    #tinh trung binh diem gan lai vao dict

        for i in dsach:
            for j in dsach[i]:
                dsach[i][j] =tinhdiem_trungbinh(dsach[i][j])
        print(dsach)
        luudiem_trungbinh(dsach)

    except:
        print("file loi")
    finally:
        f.close()
if __name__ == "__main__":
    main()
