class NhanVien():
	"""docstring for ClassName"""
	def __init__(self, name, month , salary_per_day, day_of_working, coefficient):
		self.name = name
		self.month = month
		self.salary_per_day = float(salary_per_day)
		self.day_of_working = float(day_of_working)
		self.coefficient = float(coefficient)
	
	def tinh_luong(self):
		luongTong = self.salary_per_day * self.day_of_working * self.coefficient - 1000000
		if luongTong > 9000000:
			return luongTong/100*90
		return luongTong

	def hien_thi_luong(self,a):
	    
	    print("Luong cua nhan vien {} nhan duoc trong thang {} la: {} VND".format(self.name,self.month,a))
		
class Quanly(NhanVien):
	def __init__(self, name, month , salary_per_day, day_of_working, coefficient,performance):
		super().__init__(name,month,salary_per_day,day_of_working,coefficient)
		self.performance = float(performance)

	def tinh_luong_thuong(self):
		tinh_luong_thuong = self.tinh_luong()
		if self.performance < 1:
			return tinh_luong_thuong*self.performance
		return tinh_luong_thuong*(self.performance - 1)/100*85 + tinh_luong_thuong

a = Quanly("kien",4,1000000,15,1.7,1.5)
a.hien_thi_luong(a.tinh_luong_thuong())

		