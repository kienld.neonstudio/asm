class NhanVien():
	"""docstring for ClassName"""
	def __init__(self, name, month , salary_per_day, day_of_working, coefficient):
		self.name = name
		self.month = month
		self.salary_per_day = salary_per_day
		self.day_of_working = day_of_working
		self.coefficient = coefficient
	
	def tinh_luong(self):
		luongTong = self.salary_per_day * self.day_of_working * self.coefficient - 1000000
		if luongTong > 9000000:
			return luongTong*90/100
		return luongTong

	def hien_thi_luong(self):
	    print("Luong cua nhan vien {} nhan duoc trong thang {} la: {} VND".format(self.name,self.month,self.tinh_luong()))
		
a = NhanVien("kien",3,500000,20,1.5)
a.hien_thi_luong()
