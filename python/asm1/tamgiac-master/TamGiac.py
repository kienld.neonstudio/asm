import math

# toa do A	 B     C
l = [ 5,1,   1,5,   1, 1]
#	  0   1   2  3    4  5	 
[float(i) for i in l]
 
# check tam giac
def kiemtra_tamgiac(l):
    global ab,ac,bc,a,b,c
    ab = math.sqrt( (l[0] - l[2])**2 + (l[1] - l[3])**2)
    ac = math.sqrt( (l[0] - l[4])**2 + (l[1] - l[5])**2)
    bc = math.sqrt( (l[2] - l[4])**2 + (l[3] - l[5])**2)
    
    if( (ab + ac > bc) or (ab + bc > ac) or (ab + bc > ac) ):
        a  = math.acos((ab**2 + ac**2 - bc**2 )/(2*ab*ac))
        b  = math.acos((ab**2 + bc**2 - ac**2 )/(2*ab*bc))
        c  = math.acos((ac**2 + bc**2 - ab**2 )/(2*ac*bc))
        return True
    return False
	

def goccanh_tamgiac(l):
    if kiemtra_tamgiac(l):
    	return [round(ab,2),round(ac,2) ,round(bc,2) ,round(a,2) ,round(b,2) ,round(c,2)]
    return kiemtra_tamgiac(l)
    
    
def xet_tamgiac(l):
    if kiemtra_tamgiac(l):
        # so sanh float dung xap xi :hieu 2 so < 10^-9 thi bang nhau 
        # Tam giac deu
        if(abs(ab - ac) < 1e-9 and abs(ab - bc) < 1e-9 and abs(ac - bc)< 1e-9  ):
            return "ABC la tam giac deu"
        # Tam giac vuong can
        if(abs(a - b) < 1e-9 and abs(c - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong can tai dinh C"
        if(abs(c - b) < 1e-9 and abs(a - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong can tai dinh A"
        if(abs(a - c) < 1e-9 and abs(b - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong can tai dinh B"
       
        #Tam giac tu can 
        if(abs(a - b) < 1e-9 and c - math.pi/2 >= 1e-9):
            return "ABC la tam giac can tu  tai dinh C"
        if(abs(a - c) < 1e-9 and b - math.pi/2 >= 1e-9):
            return "ABC la tam giac can tu  tai dinh B"
        if(abs(c - b) < 1e-9 and a - math.pi/2 >= 1e-9):
            return "ABC la tam giac can tu  tai dinh A"
        # tam giac can nhon can
        if(abs(c - b) < 1e-9 and math.pi/2 -a >= 1e-9):
            return "ABC la tam giac can nhon  tai dinh A"
        if(abs(c - a) < 1e-9 and math.pi/2 -b >= 1e-9):
            return "ABC la tam giac can nhon  tai dinh B"
        if(abs(a - b) < 1e-9 and math.pi/2 -c >= 1e-9):
            return "ABC la tam giac can nhon  tai dinh C"
       
        #tam giac vuong
        if(abs(c - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong tai C"
        if(abs(a - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong tai A"
        if(abs(b - math.pi/2) < 1e-9):
            return "ABC la tam giac vuong tai B"
        
        #tam giac tu
        if(c - math.pi/2 < 1e-9):
            return "ABC la tam giac tu tai C va la tam giac binh thuong"
        if(b - math.pi/2 < 1e-9):
            return "ABC la tam giac tu tai B va la tam giac binh thuong"
        if(a - math.pi/2 < 1e-9):
            return "ABC la tam giac tu tai A va la tam giac binh thuong"
        return "ABC la tam giac nhon binh thuong"
    return kiemtra_tamgiac(l)

def dientich_tamgiac(l):
    if kiemtra_tamgiac(l):
    	global s # dien tich tam giac
    	s = 0.5*ab*ac*(math.sin(a))
    	return round(s,2)
    return kiemtra_tamgiac(l)
    
# do dai duong cao 3 dinh lam tron 2 stp
def duongcao_tamgiac(l):
    if kiemtra_tamgiac(l):
    	dientich_tamgiac(l)
    	dcA = round(2*s/bc,2)
    	dcB = round(2*s/ac,2)
    	dcC = round(2*s/ab,2)
    	return [dcA, dcB, dcC]
    return kiemtra_tamgiac(l)
    
    
# do dai dinh den trong tam , lam tron 2 so thap phan
def trungtuyen_tamgiac(l):
    if kiemtra_tamgiac(l):
        tta = 1/3*math.sqrt(0.25*( 2*(ac**2 + ab**2) - bc**2))
        ttb = 1/3*math.sqrt(0.25*( 2*(ab**2 + bc**2) - ac**2))
        ttc = 1/3*math.sqrt(0.25*( 2*(ac**2 + bc**2) - ab**2))
        return [round(tta,2), round(ttb,2), round(ttc,2)]
    return kiemtra_tamgiac(l)

#toa do trong tam va truc tam [trongtam_x, trongtam_y, tructam_x, tructam_y]
def tam_tamgiac(l):
    if kiemtra_tamgiac(l):
        trongtam_x = 1/3*(l[0] + l[2] + l[4])
        trongtam_y = 1/3*(l[1] + l[3] + l[5])
        try:
            tructam_x = ((l[3]*(l[1]-l[5]) + l[2]*(l[0]-l[4])) - ((l[1]-l[5])*(l[5] *(l[1]-l[3]) + l[4]*(l[0]-l[2]))/(l[1]-l[3])))     /    (l[0] - l[4] - ((l[0]-l[2])*(l[1]-l[5])/(l[1]-l[3])))
            tructam_y = (l[5]*(l[1]-l[3]) + l[4]*(l[0]-l[2]) -tructam_x*(l[0]-l[2]) ) / (l[1] - l[3])
        except:
            if(l[1]-l[3] == 0):
                tructam_x = l[4]
                tructam_y = (-(l[0] - l[4])*(tructam_x-l[2])) / (l[1] - l[5])
        return([trongtam_x, trongtam_y, tructam_x, tructam_y])
    return kiemtra_tamgiac(l)
    
def giaima_tamgiac(l):
    if kiemtra_tamgiac(l):
        print("A, B, C hop thanh mot tam giac")
        [ab,ac,bc,a,b,c] = goccanh_tamgiac(l)
        [dcA, dcB, dcC] = duongcao_tamgiac(l)
        [ttA, ttB, ttC] = trungtuyen_tamgiac(l)
        [trongtam_x, trongtam_y, tructam_x, tructam_y] = tam_tamgiac(l)
        kq ="1.So do co ban cua tam giac:\nChieu dai canh AB: "+str(ab)+"\nChieu dai canh BC: "+str(bc)+"\nChieu dai canh CA: "+str(ac)+"\n Goc A: "+str(a)+"\n Goc b: "+str(b)+"\n Goc C: "+str(c)+"\n"+xet_tamgiac(l)+"\n2. Dien tich cua tam giac ABC: "+str(dientich_tamgiac(l))+"\n3. So do nang cao tam giac ABC:"+"\nDo dai duong cao tu dinh A: "+str(dcA)+"\nDo dai duong cao tu dinh B: "+str(dcB)+"\nDo dai duong cao tu dinh C: "+str(dcC)+"\nKhoang cach den trong tam tu dinh A: "+str(ttA)+"\nKhoang cach den trong tam tu dinh B: "+str(ttB)+"\nKhoang cach den trong tam tu dinh C: "+str(ttC)+"\n4. Toa do mot so diem dac biet cua tam giac ABC:"+"\nToa do trong tam: ["+str(trongtam_x)+","+str(trongtam_y)+"]"+"\nToa do truc tam: ["+str(tructam_x)+","+str(tructam_y)+"]"
        return(kq)
    return "A, B, C khong hop thanh mot tam giac"
    

print(giaima_tamgiac(l))

